public class Factorial {
/***
*
* Nombre: Juan Pablo Ruiz Guerra
* Carnet: 201403914
*
***/

    public static int factorial(int n) {
        int resultado = 1;
        for (int i = 1; i <= n; i++) {
            resultado *= i;
        }
        return resultado;
    }
}
