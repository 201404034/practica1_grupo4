public class Potencia_Cuadrado{
/***
*
* Nombre: Ali Basem Daryousef Tahuite
* Carnet: 201403594
*
***/

    public static int PotenciaCuadrado(int n) {
        return n*n;
    }

/***
*
* Nombre: Gembly Gabriela Gonzalez Alvarado
* Carné: 201325519
*
***/
  private static void RaizCuadrada(int i)
  {
    System.out.println("La raíz cuadrada de " + i + " es " + Math.sqrt(i));
  }

}
